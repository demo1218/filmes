# Filmes

Projeto teste feito para avaliações técnicas.
Importante lembrar que esse projeto está configurado com JAVA 17, então sua IDE deve ser compatível e ter a versão da JDK instalada.

Após inicializar o projeto spring boot, o qual pode ser feito através de um boot dashboar ou rodar a classe principal mesmo, você poderá acessar as sequintes opções:

H2 Console - para verificar os dados do banco
Swagger    - para testar os endpoints

Swagger - http://localhost:8080/swagger-ui/index.html#/
H2      - http://localhost:8080/h2-console

login e senha do H2 são
username felipe e password freire
