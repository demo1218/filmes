package br.com.freire.demo.filmes.ospiores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.freire.demo.filmes.ospiores.models.IntervaloResponse;

@RestController
@RequestMapping("/v1/filmes/piores")
public class PioresFilmesController {

	@Autowired
	private PioresFilmesFacade pioresFilmesFacade;
	
	@GetMapping("/produtores/intervalos")
	public ResponseEntity<IntervaloResponse> intervalo() {
		return this.pioresFilmesFacade.buscarIntervalo();
	}
	
}
