package br.com.freire.demo.filmes.ospiores.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PioresFilmesResponse {
	
	private String titulo;
	private String ano;
	private String estudios;
	private String produtores;
	private String vencedor;
}
