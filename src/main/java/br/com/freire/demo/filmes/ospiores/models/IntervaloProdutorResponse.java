package br.com.freire.demo.filmes.ospiores.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class IntervaloProdutorResponse {

	private String producer;
	private long interval;
	private long previousWin;
	private long followingWin;
}
