package br.com.freire.demo.filmes.config;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import br.com.freire.demo.filmes.models.FilmeEntidade;
import br.com.freire.demo.filmes.ospiores.PioresFilmesRepository;

@Service
public class PioresFilmesConfig implements ApplicationRunner{
	
	@Value("${filmes.piores.path}")
	private String path;
	
	@Autowired
	private PioresFilmesRepository repository;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		try(var linhas = Files.lines(Paths.get(path))) {
			linhas
				.skip(1)
				.map(filmes -> filmes.split(";"))
				.map(filme -> FilmeEntidade.builder()
						.ano(filme[0])
						.titulo(filme[1])
						.estudios(filme[2])
						.produtores(filme[3])
						.vencedor(filme.length == 5 ? filme[4] : null)
						.build())
				.forEach(repository::save);	
		}
		
			
	}

}
