package br.com.freire.demo.filmes.ospiores;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.freire.demo.filmes.models.FilmeEntidade;
import br.com.freire.demo.filmes.ospiores.models.IntervaloProdutorResponse;
import br.com.freire.demo.filmes.ospiores.models.IntervaloResponse;

@Service
public class PioresFilmesService {

	@Autowired
	private PioresFilmesRepository pioresFilmesRepository;
	
	public List<FilmeEntidade> buscarTodos() {
		return this.pioresFilmesRepository.findAll();
	}
	
	public IntervaloResponse buscarIntervalo() {
		
		var pioresFilmes = this.buscarTodos();
		
		
		var listaProdutores = pioresFilmes
				.stream()
				.map(FilmeEntidade::getProdutores)
				.flatMap(produtores -> Arrays.stream(produtores.split(",")))
				.flatMap(produtores -> Arrays.stream(produtores.split(" and ")))
				.map(String::trim)
				.filter(StringUtils::isNotBlank)
				.distinct()
				.toList();
		
		
		
		var listaIntervalos = new ArrayList<IntervaloProdutorResponse>();
		
		listaProdutores.stream()
			.forEach(produtor -> {
				var listaPremios = pioresFilmes.stream()
					.filter(piorFilme -> StringUtils.isNotBlank(piorFilme.getVencedor()))	
					.filter(piorFilme -> piorFilme.getProdutores().contains(produtor))
					.sorted(Comparator.comparing(FilmeEntidade::getAno))
					.toList();
				if(listaPremios.size() > 1 ) {
					listaIntervalos.add(IntervaloProdutorResponse.builder()
							.producer(produtor)
							.previousWin(Long.parseLong(listaPremios.get(0).getAno()))
							.followingWin(Long.parseLong(listaPremios.get(1).getAno()))
							.interval(Long.parseLong(listaPremios.get(1).getAno()) - Long.parseLong(listaPremios.get(0).getAno()))
							.build());
				}
			});
		
		var intervaloMinimo = listaIntervalos.stream().min(Comparator.comparingLong(IntervaloProdutorResponse::getInterval)).orElseThrow().getInterval();
		var intervaloMax = listaIntervalos.stream().max(Comparator.comparingLong(IntervaloProdutorResponse::getInterval)).orElseThrow().getInterval();
		
		
		return IntervaloResponse.builder()
				.min(listaIntervalos.stream()
						.filter(intervalo -> intervalo.getInterval() == intervaloMinimo)
						.sorted(Comparator.comparingLong(IntervaloProdutorResponse::getPreviousWin))
						.toList())
				.max(listaIntervalos.stream()
						.filter(intervalo -> intervalo.getInterval() == intervaloMax)
						.sorted(Comparator.comparingLong(IntervaloProdutorResponse::getPreviousWin))
						.toList())
				.build();
		
	}

}
