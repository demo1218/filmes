package br.com.freire.demo.filmes.ospiores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.freire.demo.filmes.ospiores.models.IntervaloResponse;
import br.com.freire.demo.filmes.ospiores.models.PioresFilmesResponse;

@Component
public class PioresFilmesFacade {

	@Autowired
	private PioresFilmesService pioresFilmesService;
	
	public ResponseEntity<List<PioresFilmesResponse>> buscarPioresFilmes() {

		 return ResponseEntity.ok(this.pioresFilmesService.buscarTodos().stream()
					.map(pior -> PioresFilmesResponse.builder()
							.titulo(pior.getTitulo())
							.ano(pior.getAno())
							.estudios(pior.getEstudios())
							.produtores(pior.getProdutores())
							.vencedor(pior.getVencedor())
							.build())
					.collect(Collectors.toList()));
	}

	public ResponseEntity<IntervaloResponse> buscarIntervalo() {
		return ResponseEntity.ok(this.pioresFilmesService.buscarIntervalo());
	}

}
