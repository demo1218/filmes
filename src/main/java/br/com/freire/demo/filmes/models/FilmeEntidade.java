package br.com.freire.demo.filmes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "FILMES")
public class FilmeEntidade {

	@Id
	@Column(name = "TITULO")
	private String titulo;
	@Column(name = "ANO")
	private String ano;
	@Column(name = "ESTUDIOS")
	private String estudios;
	@Column(name = "PRODUTORES")
	private String produtores;
	@Column(name = "VENCEDOR")
	private String vencedor;
}
