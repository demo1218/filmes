package br.com.freire.demo.filmes.ospiores;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.freire.demo.filmes.models.FilmeEntidade;

@Repository
public interface PioresFilmesRepository extends JpaRepository<FilmeEntidade, String>{

}
