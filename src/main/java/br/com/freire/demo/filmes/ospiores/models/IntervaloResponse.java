package br.com.freire.demo.filmes.ospiores.models;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IntervaloResponse {

	private List<IntervaloProdutorResponse> min;
	private List<IntervaloProdutorResponse> max;

}
